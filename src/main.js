import './style.css'

const baseUrl = "https://platzi-avo.vercel.app";
const navigationBars = document.querySelectorAll("header");
const urlParams = new URLSearchParams(window.location.search);
const productId = urlParams.get("id");
const currentPage = window.location.pathname.split('/').pop();


console.log("ID del producto:", productId);
console.log("URL actual:", window.location.href);
console.log("Query params:", window.location.search);


const formatPrice = price => {
    return new window.Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD"
    }).format(price);
};


function setupHeader() {
    navigationBars.forEach(navigationBar => {
        navigationBar.className = "flex mb-4 rounded-md shadow-sm justify-around bg-white sticky top-0 z-50 font-then";

        // Logo y enlace a inicio
        const aAvo = document.createElement("a");
        aAvo.className = "flex items-center p-3 transition duration-100 ease-in-out hover:bg-gray-200 hover:shadow-lg focus:bg-gray-300";
        aAvo.href = "index.html";
        
        const logoContent = document.createElement("div");
        logoContent.className = "flex items-center gap-2";
        logoContent.innerHTML = `
            <span>Avo Store</span>
            <img src="media/Avocado_Icon.png" class="h-10 w-10 object-contain" alt="Logo Aguacate">
        `;
        aAvo.appendChild(logoContent);

        // Enlace al carrito
        const aPanier = document.createElement("a");
        aPanier.className = "flex items-center p-3 transition duration-100 ease-in-out hover:bg-gray-200 hover:shadow-lg focus:bg-gray-300";
        aPanier.href = "cartPage.html";
        
        const cartContent = document.createElement("div");
        cartContent.className = "flex items-center gap-2";
        cartContent.innerHTML = `
            <span>Panier</span>
            <img src="media/panier.png" class="h-10 w-10 object-contain" alt="Icono Carrito">
        `;
        aPanier.appendChild(cartContent);

        navigationBar.append(aAvo, aPanier);
    });
}

function setupHomePage() {
    const appNode = document.querySelector("#app");

    async function fetchAndDisplayAvos() {
        try {
            const response = await fetch(`${baseUrl}/api/avo`);
            const { data } = await response.json();
            
            const productsHTML = data.map(item => `
                <a href="avoPage.html?id=${item.id}" class="group w-full sm:w-1/2 lg:w-1/3 p-4">
                    <div class="m-3 bg-white rounded-lg shadow-lg p-6 transition-all duration-300 hover:shadow-xl hover:scale-[1.02]">
                        <img src="${baseUrl}${item.image}" 
                             class="w-full h-48 object-cover rounded-lg"
                             loading="lazy"
                             alt="${item.name}">
                        <div class="mt-4">
                            <h2 class="text-lg font-title font-bold">${item.name}</h2>
                            <p class="mt-2 font-then text-gray-600">${formatPrice(item.price)}</p>
                        </div>
                    </div>
                </a>
            `).join('');

            appNode.innerHTML = productsHTML;
        } catch (error) {
            console.error('Error fetching avocados:', error);
            appNode.innerHTML = '<p class="text-red-500 p-4">Error cargando productos</p>';
        }
    }

    fetchAndDisplayAvos();
}

function setupProductPage(addToCart) {
    const notification = document.getElementById('notification');
    
    async function loadProductDetails() {
        try {
            const response = await fetch(`${baseUrl}/api/avo/${productId}`);
            const product = await response.json();

            console.log("Respuesta de la API:", product);
            // Actualizar datos del producto
            document.getElementById('product-name').textContent = product.name;
            document.getElementById('product-price').textContent = formatPrice(product.price);
            document.getElementById('product-description').textContent = product.attributes.description;
            document.getElementById('product-image').src = `${baseUrl}${product.image}`;
            document.getElementById('product-sku').textContent = product.sku;

            // Actualizar atributos
            const attributesTable = document.getElementById('product-attributes');
            attributesTable.innerHTML = Object.entries(product.attributes)
                .filter(([key]) => key !== 'description')
                .map(([key, value]) => `
                    <tr class="border-b">
                        <td class="px-4 py-3 font-medium">${key}</td>
                        <td class="px-4 py-3">${value}</td>
                    </tr>
                `).join('');

            // Configurar botón de añadir al carrito
            document.getElementById('add-to-cart-button').addEventListener('click', () => {
                const productToAdd = {
                    id: productId,
                    name: product.name,
                    price: product.price,
                    quantity: parseInt(document.getElementById('product-quantity').value),
                    image: `${baseUrl}${product.image}`
                };
                addToCart(productToAdd);

                // Mostrar notificación
                notification.classList.remove('opacity-0');
                setTimeout(() => notification.classList.add('opacity-0'), 2000);
            });

        } catch (error) {
            console.error('Error loading product:', error);
            document.getElementById('product-details').innerHTML = `
                <div class="p-6 text-center">
                    <p class="text-red-500 mb-4">Producto no encontrado</p>
                    <a href="index.html" class="text-blue-500 hover:underline">Volver al inicio</a>
                </div>
            `;
        }
    }
    if (productId) loadProductDetails();
    if (!productId) {
        console.error("No se proporcionó ID de producto");
        window.location.href = "index.html";
        return;
    }
    
}

function setupCartPage() {
    function updateCartDisplay() {
        const cart = JSON.parse(localStorage.getItem('cart')) || [];
        const cartItemsContainer = document.getElementById('cart-items');
        const shippingCost = 5.00;
        const taxRate = 0.16;

        if (cart.length === 0) {
            cartItemsContainer.innerHTML = '<p class="p-4 font-then text-gray-600">El carrito está vacío</p>';
            return;
        }

        // Calcular totales
        const subtotal = cart.reduce((sum, item) => sum + (item.price * item.quantity), 0);
        const tax = subtotal * taxRate;
        const total = subtotal + tax + shippingCost;

        // Actualizar UI
        cartItemsContainer.innerHTML = cart.map(item => `
            <div class="flex items-center gap-4 p-4 border-b">
                <img src="${item.image}" 
                     class="w-20 h-20 object-cover rounded-lg"
                     alt="${item.name}">
                <div class="flex-1">
                    <h3 class="font-title font-medium">${item.name}</h3>
                    <p class="font-then text-gray-600">${formatPrice(item.price)}</p>
                    <p class="font-then text-sm text-gray-500">Cantidad: ${item.quantity}</p>
                </div>
            </div>
        `).join('');

        document.getElementById('subtotal').textContent = formatPrice(subtotal);
        document.getElementById('tax').textContent = formatPrice(tax);
        document.getElementById('total').textContent = formatPrice(total);
    }

    // Botón vaciar carrito
    document.getElementById('clearCart')?.addEventListener('click', () => {
        localStorage.removeItem('cart');
        updateCartDisplay();
    });

    updateCartDisplay();
}

function setupCartSystem() {
    let cart = JSON.parse(localStorage.getItem('cart')) || [];

    function saveCart() {
        localStorage.setItem('cart', JSON.stringify(cart));
    }

    function addToCart(product) {
        const existing = cart.find(item => item.id === product.id);
        if (existing) {
            existing.quantity += product.quantity;
        } else {
            cart.push(product);
        }
        saveCart();
    }

    return { addToCart };
}

document.addEventListener('DOMContentLoaded', () => {
    setupHeader();
    const { addToCart } = setupCartSystem();

    if (currentPage === 'index.html' || currentPage === '') {
        setupHomePage();
    } else if (currentPage === 'avoPage.html') {
        setupProductPage(addToCart);
    } else if (currentPage === 'cartPage.html') {
        setupCartPage();
    }
});