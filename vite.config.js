import { defineConfig } from 'vite'
import tailwindcss from '@tailwindcss/vite'
import { resolve } from 'path';

export default defineConfig(() => ({
  plugins: [
    tailwindcss(),
  ],
  base: '/',

    build: {
      minify: 'esbuild',
      rollupOptions: {
        input: {
          main: resolve(__dirname, 'index.html'),
          cart: resolve(__dirname, 'cartPage.html'),
          product: resolve(__dirname, 'avoPage.html'),
        },
      },
      outDir: 'dist',
    },
}))

